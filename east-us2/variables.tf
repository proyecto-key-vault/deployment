variable "kvt_vm_user_secret" {
  default = "vm-user-secret"
  type    = string
}

variable "kvt_vm_pass_secret" {
  default = "vm-pass-secret"
  type    = string
}

variable "tfc_organization" {
  type = string
}