# Get Keyvault Data
data "terraform_remote_state" "west_us2" {
  backend = "remote"
  config = {
    organization = "my-company-test"
    workspaces = {
      name = "west-us2"
    }
  }
}

# Coger el id del KVT desde el tfstate remoto (Terraform Cloud)
data "azurerm_key_vault_secret" "vm_user_secret" {
  name         = var.kvt_vm_user_secret
  key_vault_id = data.terraform_remote_state.west_us2.outputs.kvt_id
}

data "azurerm_key_vault_secret" "vm_pass_secret" {
  name         = var.kvt_vm_pass_secret
  key_vault_id = data.terraform_remote_state.west_us2.outputs.kvt_id
}

data "azurerm_public_ip" "ip" {
  name                = azurerm_public_ip.publicip.name
  resource_group_name = azurerm_virtual_machine.vm.resource_group_name
  depends_on          = [azurerm_virtual_machine.vm]
}
