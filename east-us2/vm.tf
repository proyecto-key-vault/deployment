# Create Resource Group
resource "azurerm_resource_group" "rg" {
  name     = "RG-Project"
  location = "eastus2"
}

# Create a Linux virtual machine#
resource "azurerm_virtual_machine" "vm" {
  name                  = "VM-Nginx"
  location              = "eastus2"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  vm_size               = "Standard_B1s"

  storage_os_disk {
    name              = "OS_Disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "VM-Nginx"
    admin_username = data.azurerm_key_vault_secret.vm_user_secret.value
    admin_password = data.azurerm_key_vault_secret.vm_pass_secret.value
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y nginx",
    ]

    connection {
      type     = "ssh"
      user     = data.azurerm_key_vault_secret.vm_user_secret.value
      password = data.azurerm_key_vault_secret.vm_pass_secret.value
      host     = azurerm_public_ip.publicip.ip_address
    }
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

}

