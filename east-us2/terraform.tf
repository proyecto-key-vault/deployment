# Configure Azure Provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = var.tfc_organization

    workspaces {
      name = "east-us2"
    }
  }
}

provider "azurerm" {
  features {}
}