
# Seguridad con Azure Key Vault y Terraform Cloud

Basándonos en el modelo “Zero Trust“, he elegido diferentes soluciones gestionadas que nos permiten la interacción entre los diferentes servicios de una forma segura, a la vez que permite aumentar la trazabilidad de las acciones (quién, cómo y cuándo). Estas soluciones serían Bitbucket, Terraform Cloud y Azure Key Vault, que nos permiten el versionado de nuestro código, que sea reproducible, que sea automatizable y a la vez que colaborativo.

Blog --> https://azurebrains.com/

---

![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)