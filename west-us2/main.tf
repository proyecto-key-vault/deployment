resource "azurerm_resource_group" "this" {
  name     = "RG-Keyvault"
  location = "westus2"
}

resource "azurerm_key_vault" "this" {
  name                       = "tfkeyvault-test"
  location                   = azurerm_resource_group.this.location
  resource_group_name        = azurerm_resource_group.this.name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  soft_delete_enabled        = false
  soft_delete_retention_days = 7
  purge_protection_enabled   = false

  sku_name = "standard"

  # Add permissions
  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    secret_permissions = [
      "get",
      "delete",
      "list",
      "recover",
      "set",
      
    ]

  }

  tags = {
    Environment = "Deployment"
  }
}

# Generate Ramdon String
resource "random_string" "random_passwd" {
  length           = 14
  special          = true
  override_special = "/@$_"
}

# Create the secret for password
resource "azurerm_key_vault_secret" "vm_pass_secret" {
  name         = "vm-pass-secret"
  value        = random_string.random_passwd.result
  key_vault_id = azurerm_key_vault.this.id
  # vault_uri = azurerm_key_vault.this.vault_uri
}

# Create the secret for user
resource "azurerm_key_vault_secret" "vm_user_secret" {
  name         = "vm-user-secret"
  value        = var.vm_user_name
  key_vault_id = azurerm_key_vault.this.id
}
