# Configure Azure Provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = var.tfc_organization

    workspaces {
      name = "west-us2"
    }
  }
}
# Create Key Vault
provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
}
