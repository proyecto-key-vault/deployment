# Print secret data to console
output "vm_user_result" {
  value = azurerm_key_vault_secret.vm_user_secret.value
}

output "vm_passwd_result" {
  value = azurerm_key_vault_secret.vm_pass_secret.value
}

output "kvt_id" {
  value = azurerm_key_vault.this.id
}

output "kvt_uri" {
  value = azurerm_key_vault.this.vault_uri
}